// export const domain = "https://bettersystemclub.com/api/v1";

let hostname = window.location.hostname;
let apiUrl;
if (
    hostname === "admin.bettersystemclub.com" ||
    hostname === "192.168.2.42" //local dev
) {
    apiUrl = "https://bettersystemclub.com/api/v1";
} else if (hostname === "bc-admin.kisrateam.com") {
    apiUrl = "https://bettercoin0.1.kisrateam.com/api/v1";
} else {
    let path = window.location.pathname;
    let host = window.location.href;
    if(path.indexOf('maintenance') === -1) {
        window.location = `${host}maintenance`
    }
}

export const domain = apiUrl;

