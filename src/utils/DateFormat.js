const DateFormat = {
    yearMonthDay(d) {
        if (d === null) {
            return null;
        }
        // else if(typeof d === 'object')
        // {
        //     let month = '' + (d.getMonth() + 1),
        //         day = '' + d.getDate(),
        //         year = d.getFullYear();
        //
        //     if (month.length < 2) month = '0' + month;
        //     if (day.length < 2) day = '0' + day;
        //
        //     return [year, month, day].join('-');
        // }
        else {
            d = new Date(d);
            let month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hour = d.getHours(),
                minutes = d.getMinutes(),
                seconds = '00';

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hour.length < 2) day = '0' + hour;
            if (minutes.length < 2) day = '0' + minutes;

            let date = [year, month, day].join('-'),
                time = [hour, minutes, seconds].join(':');

            return date + ' ' + time;
        }
    }
};

export default DateFormat;
