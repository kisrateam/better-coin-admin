import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";
import NotFoundPage from "@/errors/404.vue";
import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";

import Items from "../pages/Items/Items";

const routes = [
    {
        path: "/login",
        name: "login",
        component: () => import("../pages/Login")
        // meta: {
        //     requiresVisitor: true
        // }
    },
    {
        path: "/404",
        name: "404",
        component: NotFoundPage
    },
    {
        path: "/",
        component: DashboardLayout,
        meta: {
            requiresAuth: true
        },
        redirect: "/dashboard",
        children: [
            {
                path: "dashboard",
                name: "Dashboard",
                component: Dashboard
            },
            {
                path: "profile",
                name: "User Profile",
                component: UserProfile
            },
            {
                path: "table",
                name: "Table List",
                component: TableList
            },
            {
                path: "typography",
                name: "Typography",
                component: Typography
            },
            {
                path: "icons",
                name: "Icons",
                component: Icons
            },
            {
                path: "maps",
                name: "Maps",
                meta: {
                    hideFooter: true
                },
                component: Maps
            },
            {
                path: "notifications",
                name: "Notifications",
                component: Notifications
            },
            {
                path: "upgrade",
                name: "Upgrade to PRO",
                component: UpgradeToPRO
            },
            /*
            |--------------------------------------------------------------------------
            | Admin Page
            |--------------------------------------------------------------------------|
            */
            {
                path: "achievements",
                name: "Achievements",
                component: () => import("../pages/Achievements/Index")
            },
            {
                path: "achievement-types",
                name: "AchievementTypes",
                component: () => import("../pages/AchievementTypes/Index")
            },
            {
                path: "categories",
                name: "Categories",
                component: () => import("../pages/Categories/Index")
            },
            {
                path: "daily-quests",
                name: "DailyQuests",
                component: () => import("../pages/DailyQuests/Index")
            },
            {
                path: "donations",
                name: "Donations",
                component: () => import("../pages/Donations/Index")
            },
            {
                path: "donation/:id?",
                name: "DonationForm",
                component: () => import("../pages/Donations/Form")
            },
            {
                path: "events",
                name: "Events",
                component: () => import("../pages/Events/Index")
            },
            {
                path: "items",
                name: "Items",
                component: Items
            },
            {
                path: "item/:id?",
                name: "ItemForm",
                component: () => import("../pages/Items/ItemForm")
            },
            {
                path: "location-quests",
                name: "LocationQuests",
                component: () => import("../pages/LocationQuests/Index")
            },
            {
                path: "login-quests",
                name: "LoginQuests",
                component: () => import("../pages/LoginQuests/Index")
            },
            {
                path: "login-quest/:id?",
                name: "LoginQuestsForm",
                component: () => import("../pages/LoginQuests/Form")
            },
            {
                path: "news",
                name: "News",
                component: () => import("../pages/News/Index")
            },
            {
                path: "new/:id?",
                name: "NewForm",
                component: () => import("../pages/News/Form")
            },
            {
                path: "ranking",
                name: "Ranking",
                component: () => import("../pages/Ranking/Index")
            },
            {
                path: "reward-sets",
                name: "RewardSets",
                component: () => import("../pages/RewardSets/Index")
            },
            {
                path: "system-configs",
                name: "SystemConfigs",
                component: () => import("../pages/SystemConfigs/Index")
            },
            {
                path: "users",
                name: "User",
                component: () => import("../pages/Users/Index")
            },
            {
                path: "user/history/:user_id/steps",
                name: "StepsHistory",
                component: () => import("../pages/Users/History/Steps")
            },
            {
                path: "user/history/:user_id/purchase",
                name: "PurchaseHistory",
                component: () => import("../pages/Users/History/Purchase")
            },
            {
                path: "verify",
                name: "Verify",
                component: () => import("../pages/Verify/Index")
            },
            {
                path: "zzz",
                name: "Zzz",
                component: () => import("../Test")
            }
        ]
    },
    /*
    |--------------------------------------------------------------------------
    | Default Routes
    |--------------------------------------------------------------------------|
    */
    {
        path: "*",
        component: NotFoundPage
    }
];

export default routes;
