import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Auth from "./Modules/Auth";
import Item from "./Modules/Items";

// window.localStorage.removeItem("vuex");
Vue.use(Vuex);

const modules = {
  Auth: Auth,
  Item: Item
};

export default new Vuex.Store({
  modules,
  plugins: [createPersistedState()]
});
