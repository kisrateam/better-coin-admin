import Service from "../../services";

const itemModule = {
  namespaced: true,
  state: {
    selectedItem: {
      active: "active",
      amount: null,
      better_price: null,
      can_purchase: null,
      category_id: null,
      description: null,
      duration: null,
      effects: null,
      ended_at: null,
      expired_time: null,
      id: null,
      image: null,
      money_price: null,
      name: null,
      type: null,
      verified_required: null
    }
  },
  mutations: {
    selectedItemData: (state, item) => {
      state.selectedItem = item;
    }
  },
  getters: {
    getSelectedItem(state) {
      return state.selectedItem;
    }
  },
  actions: {
    setSelectedItemData: function({ commit }, item) {
      commit("selectedItemData", item);
    }
  }
};

export default itemModule;
