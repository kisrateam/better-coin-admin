import Service from "../../services";

const authModule = {
    namespaced: true,
    state: {
        user: null,
        accessToken: null
    },
    mutations: {
        SET_TOKEN: (state, token) => {
            state.accessToken = token;
        },

        SET_USER: (state, user) => {
            state.user = user;
        }
    },
    actions: {
        login({commit}, data) {
            return Service.auth
                .login(data)
                .then(data => {
                    commit("SET_USER", data)
                    commit("SET_TOKEN", data.result.access_token);
                    return data;
                })
                .catch(err => alert("Invalid !! " + err.message));
        },
        setUser({commit, state}, userId) {
            if (state.accessToken == null) {
                return Promise.reject("No access token.");
            }

            return Service.auth
                .getUser(userId)
                .then(data => commit("SET_USER", data))
                .catch(err => Promise.reject(err));
        },
        logout({commit}) {
            return Service.auth.logout().then(() => {
                commit("SET_USER", null);
                commit("SET_TOKEN", null);
            });
        }
    }
};

export default authModule;
