import Service from "../AuthService";

const events = {
    get() {
        return Service.get(`/admin/event`);
    },
    show(id) {
        return Service.get(`/admin/event/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/event/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/event`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/event/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/event?page=${page}`);
    }
};

export default events;
