import Service from "../AuthService";

const AchievementTypes = {
  get() {
    return Service.get(`/admin/achievementType`);
  },
  show(id) {
    return Service.get(`/admin/achievementType/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/achievementType/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/achievementType`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/achievementType/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/achievementType?page=${page}`);
  }
};

export default AchievementTypes;
