import Service from "../AuthService";

const Users = {
    get() {
        return Service.get(`/admin/user`);
    },
    show(id) {
        return Service.get(`/admin/user/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/user/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/user`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/user/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/user?page=${page}`);
    },
    history(user_id, type){
        return Service.get(`/admin/history/${user_id}/${type}`);
    },
    historyByPage(user_id, type, page){
        return Service.get(`/admin/history/${user_id}/${type}?page=${page}`);
    }
};

export default Users;
