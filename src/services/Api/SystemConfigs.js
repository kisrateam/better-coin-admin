import Service from "../AuthService";

const Configs = {
  get() {
    return Service.get(`/admin/config`);
  },
  show(id) {
    return Service.get(`/admin/config/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/config/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/config`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/config/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/config?page=${page}`);
  }
};

export default Configs;
