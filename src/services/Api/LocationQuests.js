import Service from "../AuthService";

const locationQuests = {
    get() {
        return Service.get(`/admin/locationQuests`);
    },
    show(id) {
        return Service.get(`/admin/locationQuests/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/locationQuests/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/locationQuests`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/locationQuests/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/locationQuests?page=${page}`);
    }
};

export default locationQuests;
