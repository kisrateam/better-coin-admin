import Service from "../AuthService";

const RewardSets = {
    get(key = "") {
        return Service.get(`/admin/rewardset?search=${key}`);
    },
    show(id) {
        return Service.get(`/admin/rewardset/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/rewardset/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/rewardset`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/rewardset/${id}`);
    },
    getByPage(page, key = "") {
        return Service.get(`/admin/rewardset?page=${page}&search=${key}`);
    },
    selectOption() {
        return Service.get(`/admin/select-option/rewardset`);
    }
};

export default RewardSets;
