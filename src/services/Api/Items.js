import Service from "../AuthService";

const Items = {
  getAllItems( key = "") {
    return Service.get(`/items?search=${key}`);
  },
  getItemDetails(item_id) {
    return Service.get(`/items/${item_id}`);
  },
  post(payload) {
    return Service.post(`/admin/item`, payload);
  },
  putUpdateItem(item_id, payload) {
    return Service.put(`/admin/item/${item_id}`, payload);
  },
  deleteItem(item_id) {
    return Service.delete(`/admin/item/${item_id}`);
  },
  getItemByPage(page, key = "") {
    return Service.get(`/items?page=${page}&search=${key}`);
  },
  updateRedeemStatus(payload) {
    return Service.post(`/admin/redeem/send`, payload);
  }
};

export default Items;
