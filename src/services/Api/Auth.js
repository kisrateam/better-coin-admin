import Service from "../Service";
import AuthService from "../AuthService";

const Auth = {
  login(data) {
    return Service.post("/auth/admin/login", data);
  },

  getUser(userId) {
    return AuthService.get("/user/" + userId);
  },

  logout() {
    return AuthService.get("auth/logout");
  }
};

export default Auth;
