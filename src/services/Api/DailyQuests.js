import Service from "../AuthService";

const dailyQuests = {
    get() {
        return Service.get(`/admin/dailyQuests`);
    },
    show(id) {
        return Service.get(`/admin/dailyQuests/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/dailyQuests/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/dailyQuests`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/dailyQuests/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/dailyQuests?page=${page}`);
    }
};

export default dailyQuests;
