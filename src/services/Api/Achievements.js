import Service from "../AuthService";

const Achievements = {
  get() {
    return Service.get(`/admin/achievement`);
  },
  show(id) {
    return Service.get(`/admin/achievement/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/achievement/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/achievement`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/achievement/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/achievement?page=${page}`);
  }
};

export default Achievements;
