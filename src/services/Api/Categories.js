import Service from "../AuthService";

const Categories = {
  get() {
    return Service.get(`/admin/categories`);
  },
  show(id) {
    return Service.get(`/admin/categories/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/categories/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/categories`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/categories/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/categories?page=${page}`);
  }
};

export default Categories;
