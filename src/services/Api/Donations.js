import Service from "../AuthService";

const Donations = {
  get() {
    return Service.get(`/admin/donations`);
  },
  show(id) {
    return Service.get(`/admin/donations/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/donations/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/donations`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/donations/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/donations?page=${page}`);
  }
};

export default Donations;
