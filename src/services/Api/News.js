import Service from "../AuthService";

const News = {
  get() {
    return Service.get(`/admin/news`);
  },
  show(id) {
    return Service.get(`/admin/news/${id}`);
  },
  put(id, payload) {
    return Service.put(`/admin/news/${id}`, payload);
  },
  post(payload) {
    return Service.post(`/admin/news`, payload);
  },
  delete(id) {
    return Service.delete(`/admin/news/${id}`);
  },
  getByPage(page) {
    return Service.get(`/admin/news?page=${page}`);
  }
};

export default News;
