import Service from "../AuthService";

const VerifyUser = {
    get(key = "") {
        return Service.get(`/admin/verification/user?search=${key}`);
    },
    show(id) {
        return Service.get(`/admin/verification/user/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/verification/user/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/verification/verify`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/verification/user/${id}`);
    },
    getByPage(page, key = "") {
        return Service.get(`/admin/verification/user?page=${page}&search=${key}`);
    },
};

export default VerifyUser;
