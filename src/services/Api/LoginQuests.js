import Service from "../AuthService";

const loginQuests = {
    get() {
        return Service.get(`/admin/loginQuests`);
    },
    show(id) {
        return Service.get(`/admin/loginQuests/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/loginQuests/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/loginQuests`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/loginQuests/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/loginQuests?page=${page}`);
    },
    showDayQuest(id) {
        return Service.get(`/admin/loginDayQuest/${id}`);
    },
    putDayQuest(id, payload) {
        return Service.put(`/admin/loginDayQuest/${id}`, payload);
    },
    postDayQuest(payload) {
        return Service.post(`/admin/loginDayQuest`, payload);
    },
    deleteDayQuest(id){
        return Service.delete(`/admin/loginDayQuest/${id}`);
    }
};

export default loginQuests;
