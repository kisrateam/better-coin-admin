import monthly from "./Monthly/Index";
import weekly from "./Weekly/Index";
export default {
    monthly,
    weekly,
};
