import Service from "../../../AuthService";

const Reward = {
    get() {
        return Service.get(`/admin/weeklyReward`);
    },
    show(id) {
        return Service.get(`/admin/weeklyReward/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/weeklyReward/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/weeklyReward`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/weeklyReward/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/weeklyReward?page=${page}`);
    }
};

export default Reward;
