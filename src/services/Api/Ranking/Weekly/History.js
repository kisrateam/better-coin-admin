import Service from "../../../AuthService";

const History = {
    get(start = null, end = null) {
        return Service.get(`/admin/weekly`,{
            params: {
                started_at: start,
                ended_at:end
            }
        });
    },
    show(id) {
        return Service.get(`/admin/weekly/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/weekly/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/weekly`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/weekly/${id}`);
    },
    getByPage(page,start = null, end = null) {
        return Service.get(`/admin/weekly?page=${page}`,{
            params: {
                started_at: start,
                ended_at:end
            }
        });
    },
    getCurrent(){
        return Service.get(`/rank/weekly`);
    }
};

export default History;
