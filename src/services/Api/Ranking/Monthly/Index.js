import history from './History';
import reward from './Reward';

export default {
    history,
    reward
}