import Service from "../../../AuthService";

const Reward = {
    get() {
        return Service.get(`/admin/monthlyReward`);
    },
    show(id) {
        return Service.get(`/admin/monthlyReward/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/monthlyReward/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/monthlyReward`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/monthlyReward/${id}`);
    },
    getByPage(page) {
        return Service.get(`/admin/monthlyReward?page=${page}`);
    }
};

export default Reward;
