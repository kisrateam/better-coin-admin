import Service from "../../../AuthService";

const History = {
    get(month = null, year = null) {
        return Service.get(`/admin/monthly`,{
            params: {
                month: month,
                year:year
            }
        });
    },
    show(id) {
        return Service.get(`/admin/monthly/${id}`);
    },
    put(id, payload) {
        return Service.put(`/admin/monthly/${id}`, payload);
    },
    post(payload) {
        return Service.post(`/admin/monthly`, payload);
    },
    delete(id) {
        return Service.delete(`/admin/monthly/${id}`);
    },
    getByPage(page,month = null, year = null) {
        return Service.get(`/admin/monthly?page=${page}`,{
            params: {
                month: month,
                year:year
            }
        });
    },
    getCurrent(){
        return Service.get(`/rank/monthly`);
    }
};

export default History;
