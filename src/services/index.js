import loginQuests from "./Api/LoginQuests"
import events from "./Api/Events"
import locationQuests from "./Api/LocationQuests";
import dailyQuests from "./Api/DailyQuests";
import ranking from "./Api/Ranking";
import users from "./Api/Users";
import rewardSets from "./Api/RewardSets";
import configs from "./Api/SystemConfigs";
import donations from "./Api/Donations";
import news from "./Api/News";
import achievements from "./Api/Achievements";
import achievementTypes from "./Api/AchievementTypes";
import categories from "./Api/Categories";
import items from "./Api/Items";
import auth from "./Api/Auth";
import verifyUser from "./Api/VerifyUser";

export default {
    auth,
    items,
    categories,
    achievementTypes,
    achievements,
    news,
    donations,
    configs,
    rewardSets,
    users,
    ranking,
    dailyQuests,
    locationQuests,
    events,
    loginQuests,
    verifyUser
};
