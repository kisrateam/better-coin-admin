// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App";
import VueResource from "vue-resource";

// router setup
import routes from "./routes/routes";

// Plugins
import GlobalComponents from "./globalComponents";
import GlobalDirectives from "./globalDirectives";
import Notifications from "./components/NotificationPlugin";

// MaterialDashboard plugin
import MaterialDashboard from "./material-dashboard";

import Chartist from "chartist";
import store from "./store/index";

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  mode: "history",
  linkExactActiveClass: "nav-item active"
});

import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";
import "vue-ads-pagination/dist/vue-ads-pagination.css";
import Auth from "./services/Api/Auth";
import DateFormat from "./utils/DateFormat";

import "./assets/scss/better-coin.scss"


Vue.use(VueMaterial);
Vue.use(VueRouter);
Vue.use(MaterialDashboard);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);
Vue.use(DateFormat);

Vue.prototype.$Chartist = Chartist;

Vue.use(VueResource);

Vue.http.headers.common["Content-Type"] = "application/json";
Vue.http.headers.common["Access-Control-Allow-Origin"] = "*";
Vue.http.headers.common["Accept"] = "application/json, text/plain, */*";
Vue.http.headers.common["Access-Control-Allow-Headers"] =
  "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin";

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (store.state.Auth.accessToken == null) {
      next({
        name: "login"
      });
    } else {
      Auth.getUser(store.state.Auth.user.result.id)
        .then(() => {
          next();
        })
        .catch(() => {
          window.localStorage.removeItem("vuex");
          next({
            name: "login"
          });
        });
    }
  } else {
    next(); // make sure to always call next()!
  }
});

/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: h => h(App),
  router,
  store,

  data: {
    Chartist: Chartist
  }
});
